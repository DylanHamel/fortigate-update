#!/usr/bin/env python

import ipaddress
import getopt
import sys
import time

#
#
#
#
#
def getOptForFortigateUpdate(argv) :
    """
    -h => help
    -b => download image with a usb key can only be setted if -v is setted
    

    [xxx] = default value
    -u [admin]          => username 
    -p []               => password
    -i [192.168.1.99]   => Fortigate ipAdresse
    -a [443]            => https port (default 443)
    -s [22]             => ssh port (default 22)
    -v [0.0.0]          => version

    -f [0.0.0.0]        => define ftp server ip address   can only be setted if -v is setted
    -t [0.0.0.0]        => define tftp server ip address  can only be setted if -v is setted
    -e [admin]          => FTP user
    -g [password]       => FTP password


    :param argv:
    :return:
    """

    print("========================================================================")
    
    # Default Value
    argumentsValue = dict()
    argumentsValue["username"] = "admin"
    argumentsValue["password"] = ""
    argumentsValue["ipAddress"] = "192.168.1.99"
    argumentsValue["update"] = False
    argumentsValue["version"] = "0.0.0"
    argumentsValue["usb"] = True
    argumentsValue["tftpServer"] = "0.0.0.0"
    argumentsValue["tftpServerUse"] = False
    argumentsValue["ftpServer"] = "0.0.0.0"
    argumentsValue["ftpServerUse"] = False
    argumentsValue["ftpUser"] = "admin"
    argumentsValue["ftpPassword"] = "password"
    argumentsValue["httpsPort"] = 443
    argumentsValue["sshPort"] = 22


    # Block try for block others options
    try:
        print("[getOptForFortigateUpdate] argv - ",  argv)
        opts, args = getopt.getopt(sys.argv[1:], 'hv:u:p:i:a:s:f:t:g:e:', ['--help'])

    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(1)
    except Exception as err :
        print("Warnings parameters are missing or are not correct")
        print("use -h for help")
        sys.exit(1)
        
    # Set value with values
    for o, a in opts :
        if o == "-u":
            argumentsValue["username"] = a
        elif o == "-p":
            argumentsValue["password"] = a
        elif o == "-i":
            argumentsValue["ipAddress"] = a
            assert ipaddress.IPv4Address(a)
        elif o == "-v" :
            argumentsValue["version"] =  "v" + a
            argumentsValue["update"] = True
        elif o == "-t" :
            argumentsValue["tftpServer"] = a
            assert ipaddress.IPv4Address(a)
            argumentsValue["tftpServerUse"] = True
            argumentsValue["usb"] = False
        elif o == "-f" :
            argumentsValue["ftpServer"] = a
            assert ipaddress.IPv4Address(a)
            argumentsValue["ftpServerUse"] = True
            argumentsValue["usb"] = False
        elif o == "-e":
            argumentsValue["ftpUser"] = a
        elif o == "-g":
            argumentsValue["ftpPassword"] = a
        elif o == "-a" :
            assert (int(a) < 65536 and int(a) > 0)
            argumentsValue["httpsPort"] = a
        elif o == "-s" :
            assert (int(a) < 65536 and int(a) > 0)
            argumentsValue["sshPort"] = a

        elif o in ("-h" or "--help"):
            print("You can run the script with the following arguments :")
            print("[xxx] = default value \n \
            -u [admin]          => username \n \
            -p []               => password \n \
            -i [192.168.1.99]   => Fortigate ipAdresse \n \
            -a [443]            => https port (default 443) \n \
            -s [22]             => ssh port (default 22) \n \
            -v [0.0.0]          => version \n \n \
            -f [0.0.0.0]        => define ftp server ip address   can only be setted if -v is setted \n \
            -t [0.0.0.0]        => define tftp server ip address  can only be setted if -v is setted \n \
            -e [admin]          => FTP user \n \
            -g [password]       => FTP password \n")
            sys.exit(1)


    if (argumentsValue["update"] is True and argumentsValue["usb"] is False and "0.0.0.0" in argumentsValue["tftpServer"] and "0.0.0.0" in argumentsValue["ftpServer"]):
        print("[getOptForFortigateUpdate] WARNING !!!! \n \
        You would like update your fortigate but you don't specify where download the image \n \
        Your fortigate will not be updated")
        argumentsValue["update"] = False
        time.sleep(3)
        

    print("[getOptForFortigateUpdate] username   -", argumentsValue["username"])
    print("[getOptForFortigateUpdate] password   -", argumentsValue["password"])
    print("[getOptForFortigateUpdate] ipAdress   -", argumentsValue["ipAddress"])
    print("[getOptForFortigateUpdate] httpsPort  -", argumentsValue["httpsPort"])
    print("[getOptForFortigateUpdate] sshPort    -", argumentsValue["sshPort"])
    print("[getOptForFortigateUpdate] usbKey     -", argumentsValue["usb"])
    print("[getOptForFortigateUpdate] tftpServer -", argumentsValue["tftpServer"])
    print("[getOptForFortigateUpdate] ftpServer  -", argumentsValue["ftpServer"])
    print("[getOptForFortigateUpdate] ftpUser    -", argumentsValue["ftpUser"])
    print("[getOptForFortigateUpdate] ftpPassword-", argumentsValue["ftpPassword"])
    print("[getOptForFortigateUpdate] update     -", argumentsValue["update"])
    print("[getOptForFortigateUpdate] version    -", argumentsValue["version"])

    return argumentsValue
# --------------------------------------------------------------------------------------------------
#
#
#
if __name__ == "__main__":
   getOptForFortigateUpdate(sys.argv[1:])

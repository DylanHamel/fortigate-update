"""
This Class is an API for get forigate informations
"""

# For HTTP/S requests
import requests
import urllib3
# ------------------------------------------------------------------------------------------------------------
#
#
#
#
class FortigateAPI :
    """
    FortigateAPI object parameters
    * Fortigate username
    * Fortigate password
    * Fortigate connexion ipAddress
    * Fortigate connexion port
    * Cookie
    * Cookies connexion
    * Connexion method if https ou http
    * Fortigate Version     Example v6.0.0.
    * Fortigate Model       Example 81E
    """

    def getModel(self):
        model = "000"

        if self.connected is True :
            self.connect()
            print("========================================================================")
            # Default value
            path = "/api/v2/monitor/system/firmware"
            model = "Error"

            print("[FortigateAPI getModel] - ", self._connectionMethodAddress + ":" + str(self._port) + path)
            
            get = requests.get(self._connectionMethodAddress + ":" + str(self._port) + path ,
                            verify=self._sslCertificat, cookies=self._cookies)

            getJSON = get.json()
            print("[FortigateAPI getModel] model   - ", getJSON["results"]["current"]["platform-id"][3:])
            model = getJSON["results"]["current"]["platform-id"][3:]
        
        return model
    
    # --------------------------------------------------------------------------------------------------
    #
    #
    #
    def getVersion(self):
        version = "0.0.0"
        self.connect()
        print("========================================================================")
        if self.connected is True:
            # Default value
            path = "/api/v2/monitor/system/firmware"
            version = "Error"

            print("[FortigateAPI getVersion] - ", self._connectionMethodAddress + ":" + str(self._port) + path)
            get = requests.get(self._connectionMethodAddress + ":" + str(self._port) + path ,
                            verify=self._sslCertificat, cookies=self._cookies)
            getJSON = get.json()
            print("[FortigateAPI getVersion] version - ",getJSON["version"])
            version = getJSON["version"]
        
        return version
    # --------------------------------------------------------------------------------------------------
    #
    #
    #
    def connect(self):
        # Connexion on Fortigate
        print("========================================================================")
        print("[FortigateAPI - connect] requests - ", self._connectionMethodAddress + ":" + str(self._port) + '/logincheck')

        try:
            # For avoid InsecureRequestWarning error
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            request = requests.post(self._connectionMethodAddress + ":" + str(self._port) + '/logincheck', self._usernamePassword, verify=self._sslCertificat)
            self.connected = True
            print("[FortigateAPI - connect] Cookies - ", request.cookies)
            self._cookies = request.cookies

            for c in self._cookies :
                self._listCookies.append(c)
            self.connected = True
        except Exception as error:
            print("[FortigateAPI - connect] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            print("[FortigateAPI - connect] ERROR - IMPOSSIBLE TO CONNECT ON FORTIGATE DEVICE")
            print("[FortigateAPI - connect] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            self.connected = False


    # --------------------------------------------------------------------------------------------------
    #
    #
    #
    def __init__ (self, username, password, ipAddress, port=99999, sslCertificat=False, useHTTPS=False) :
        """
        :param username:        Fortigate username
        :param password:        Fortigate password
        :param ipAddress:       Fortigate ipAddress
        :param port:            Fortigate port
        :param sslCertificat:   Fortigate sslCertificat
        :param useHTTPS:        Fortigate useHTTPS true if HTTPS is used
        """
        
        # Check for avoid ssl =! TRUE or FALSE.
        # assert useHTTPS == False or useHTTPS == True
        # assert sslCertificat == False or sslCertificat == True

        print("[FortigateAPI - __init__] - ", username, password, ipAddress, port)

        self._ipAddress = ipAddress
        self._username = username
        self._password = password
        self._ipAddress = ipAddress
        self._listCookies = list()
        self._usernamePassword = {'username': username, 'secretkey': password}
        self._sslCertificat = sslCertificat
        self.connected = True

        # We define connexion method
        if useHTTPS :
            self._connectionMethodAddress = "https://" + ipAddress
            if port == 99999 :
                self._port = 443
            else :
                self._port = port

        else:
            self._connectionMethodAddress = "http://" + ipAddress
            if port == 99999 :
                self._port = 80
            else :
                self._port = port
# Fortigate-Update

This script will update your fortigate

### Requisites

```bash
pip install --upgrade pip
pip install click
pip install netmiko
```

### Run the script

```bash
(master)⚡ % ./FortigateUpdate.py -h                                                                                                        /Volumes/Data/prog/fortigate-update
========================================================================
[getOptForFortigateUpdate] argv -  ['-h']
You can run the script with the following arguments :
[xxx] = default value
-u [admin]          => username
-p []               => password
-i [192.168.1.99]   => Fortigate ipAdresse
-a [443]            => https port (default 443)
-s [22]             => ssh port (default 22)
-v [0.0.0]          => version

-f [0.0.0.0]        => define ftp server ip address   can only be setted if -v is setted
-t [0.0.0.0]        => define tftp server ip address  can only be setted if -v is setted
-e [admin]          => FTP user
-g [password]       => FTP password
```

### Script output

```bash
(master)⚡ [130] % ./FortigateUpdate.py -v 6.0.2                                                                                            /Volumes/Data/prog/fortigate-update
========================================================================
[getOptForFortigateUpdate] argv -  ['-v', '6.0.2']
[getOptForFortigateUpdate] username   - admin
[getOptForFortigateUpdate] password   -
[getOptForFortigateUpdate] ipAdress   - 192.168.1.99
[getOptForFortigateUpdate] httpsPort  - 443
[getOptForFortigateUpdate] sshPort    - 22
[getOptForFortigateUpdate] usbKey     - True
[getOptForFortigateUpdate] tftpServer - 0.0.0.0
[getOptForFortigateUpdate] ftpServer  - 0.0.0.0
[getOptForFortigateUpdate] ftpUser    - admin
[getOptForFortigateUpdate] ftpPassword- password
[getOptForFortigateUpdate] update     - True
[getOptForFortigateUpdate] version    - v6.0.2
========================================================================
[FortigateUpdate checkIfPortOpen] IP:Port - 192.168.1.99 : 443
[FortigateUpdate checkIfPortOpen] mySocket - <socket.socket fd=5, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=0, laddr=('0.0.0.0', 0)>
[FortigateUpdate checkIfPortOpen] port 443 is open
[FortigateUpdate checkIfPortOpen] IP:Port - 192.168.1.99 : 22
[FortigateUpdate checkIfPortOpen] mySocket - <socket.socket fd=5, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=0, laddr=('0.0.0.0', 0)>
[FortigateUpdate checkIfPortOpen] port 22 is open
PING 192.168.1.99 (192.168.1.99): 56 data bytes
64 bytes from 192.168.1.99: icmp_seq=0 ttl=255 time=0.477 ms

--- 192.168.1.99 ping statistics ---
1 packets transmitted, 1 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 0.477/0.477/0.477/0.000 ms
[FortigateUpdate - main] PortAreOpen - True
[FortigateUpdate - main] PING_WORKS  - True
[FortigateAPI - __init__] -  admin  192.168.1.99 443
========================================================================
[FortigateAPI - connect] requests -  https://192.168.1.99:443/logincheck
[FortigateAPI - connect] Cookies -  <RequestsCookieJar[<Cookie APSCOOKIE_655588332="Era%3D1%26Payload%3DxnHCYtOvq973vVzXBbeBtmA%2FQnAsLnv0SIR+%2FtRg7Dp7bcIJuoDcdzVMkKBz1ZC2%0ANKoGJOK0mJzDajJzcObuBMNSQD2VL2Jdw9IQ2v4FxZJ0ZbBwtdObnDSf0sWSYJCf%0AaNcHHS7CYLKBk+6+D1pCcQ%3D%3D%0A%26AuthHash%3DVNFuaEI9%2F%2FVfkyx4qiLy721%2FKfkA%0A" for 192.168.1.99/>, <Cookie ccsrftoken="4C321F05D5C67B56E426EC65FF76D" for 192.168.1.99/>, <Cookie ccsrftoken_655588332="4C321F05D5C67B56E426EC65FF76D" for 192.168.1.99/>]>
========================================================================
[FortigateAPI getModel] -  https://192.168.1.99:443/api/v2/monitor/system/firmware
[FortigateAPI getModel] model   -  60D
========================================================================
[FortigateAPI - connect] requests -  https://192.168.1.99:443/logincheck
[FortigateAPI - connect] Cookies -  <RequestsCookieJar[<Cookie APSCOOKIE_655588332="Era%3D1%26Payload%3DxnHCYtOvq973vVzXBbeBtmA%2FQnAsLnv0SIR+%2FtRg7Dp7bcIJuoDcdzVMkKBz1ZC2%0ACwvRgpQMiFOCaZTJlGWGRuSi%2FLw5wRD32HmBD6wjqUT2Kgp+AV4nzHIz0aHNBWtX%0AMj10h95403v8kyTK2E1SIw%3D%3D%0A%26AuthHash%3D34mUt6T35+1da14GuVmAyGX+NwsA%0A" for 192.168.1.99/>, <Cookie ccsrftoken="4C611797A713478F643467A97BE57753" for 192.168.1.99/>, <Cookie ccsrftoken_655588332="4C611797A713478F643467A97BE57753" for 192.168.1.99/>]>
========================================================================
[FortigateAPI getVersion] -  https://192.168.1.99:443/api/v2/monitor/system/firmware
[FortigateAPI getVersion] version -  v6.0.1
[FortigateUpdate - main] -  60D
========================================================================
[getFortigateFirmwareFilename] Receive - | 60D | and | v6.0.2 |
[getFortigateFirmwareFilename] modelInList - 40C
[getFortigateFirmwareFilename] modelInList - False
[getFortigateFirmwareFilename] modelInList - 30E
[getFortigateFirmwareFilename] modelInList - False
[getFortigateFirmwareFilename] modelInList - 60D
[getFortigateFirmwareFilename] modelInList - True
[getFortigateFirmwareFilename] goodModel - True
[getFortigateFirmwareFilename] Return - | FGT_60D-v6-build0163-FORTINET.out | and | True |
[FortigateUpdate - main] -  ('FGT_60D-v6-build0163-FORTINET.out', True)
########################################################################
========================================================================
[getFortigateFirmwareFilename] Receive - | 60D | and | v6.0.2 |
[getFortigateFirmwareFilename] modelInList - 40C
[getFortigateFirmwareFilename] modelInList - False
[getFortigateFirmwareFilename] modelInList - 30E
[getFortigateFirmwareFilename] modelInList - False
[getFortigateFirmwareFilename] modelInList - 60D
[getFortigateFirmwareFilename] modelInList - True
[getFortigateFirmwareFilename] goodModel - True
[getFortigateFirmwareFilename] Return - | FGT_60D-v6-build0163-FORTINET.out | and | True |
[FortigateUpdate - update] FGT_60D-v6-build0163-FORTINET.out  For Fortigate  60D  Version  v6.0.1  Upgrade to  v6.0.2
[FortigateUpdate - update] command - execute restore image usb FGT_60D-v6-build0163-FORTINET.out
[FortigateUpdate - update] output  - execute restore image usb FGT_60D-v6-build0163-FORTINET.out
This operation will replace the current firmware version!
Do you want to continue? (y/n)y^J

Please wait...

Copy image FGT_60D-v6-build0163-FORTINET.out from USB disk ...

PING 192.168.1.99 (192.168.1.99): 56 data bytes

--- 192.168.1.99 ping statistics ---
1 packets transmitted, 0 packets received, 100.0% packet loss
[FortigateUpdate - update] Device down ...
PING 192.168.1.99 (192.168.1.99): 56 data bytes

--- 192.168.1.99 ping statistics ---
1 packets transmitted, 0 packets received, 100.0% packet loss
[FortigateUpdate - update] Device down ...
PING 192.168.1.99 (192.168.1.99): 56 data bytes

--- 192.168.1.99 ping statistics ---
1 packets transmitted, 0 packets received, 100.0% packet loss
[FortigateUpdate - update] Device down ...
PING 192.168.1.99 (192.168.1.99): 56 data bytes
36 bytes from vl4-br02.gva.dfinet.net (195.70.0.4): Destination Host Unreachable
Vr HL TOS  Len   ID Flg  off TTL Pro  cks      Src      Dst
 4  5  00 5400 34c1   0 0000  3d  01 e79a 10.74.148.248  192.168.1.99


--- 192.168.1.99 ping statistics ---
1 packets transmitted, 0 packets received, 100.0% packet loss
[FortigateUpdate - update] Device down ...
PING 192.168.1.99 (192.168.1.99): 56 data bytes

--- 192.168.1.99 ping statistics ---
1 packets transmitted, 0 packets received, 100.0% packet loss
[FortigateUpdate - update] Device down ...
PING 192.168.1.99 (192.168.1.99): 56 data bytes

--- 192.168.1.99 ping statistics ---
1 packets transmitted, 0 packets received, 100.0% packet loss
[FortigateUpdate - update] Device down ...
PING 192.168.1.99 (192.168.1.99): 56 data bytes

--- 192.168.1.99 ping statistics ---
1 packets transmitted, 0 packets received, 100.0% packet loss
[FortigateUpdate - update] Device down ...
PING 192.168.1.99 (192.168.1.99): 56 data bytes
64 bytes from 192.168.1.99: icmp_seq=0 ttl=255 time=0.718 ms

--- 192.168.1.99 ping statistics ---
1 packets transmitted, 1 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 0.718/0.718/0.718/0.000 ms
[FortigateUpdate - update] Device is now reachable !
########################################################################
[FortigateAPI - __init__] -  admin  192.168.1.99 443
========================================================================
[FortigateAPI - connect] requests -  https://192.168.1.99:443/logincheck
[FortigateAPI - connect] Cookies -  <RequestsCookieJar[<Cookie APSCOOKIE_655588332="Era%3D1%26Payload%3D1SGbcFdRvzW%2FDqy7RHwLuKEfmJ+pvz+OavOsJNF%2Fjp6+Wkw7NeCcaGslIkxYRxoG%0AWgLmrScr59VG+LsOgaqCNzL6Se82BXWyXOxtB3zNFou2dCxqqqalbeco78oYV7Bw%0AMhqGXUBT+IWwL6Tx8KFy%2Fw%3D%3D%0A%26AuthHash%3D20MJU3o%2FlwzRRXIbfipHMCAcBxkA%0A" for 192.168.1.99/>, <Cookie ccsrftoken="BC221EEE9A23A1F95FE2E864E310CB3" for 192.168.1.99/>, <Cookie ccsrftoken_655588332="BC221EEE9A23A1F95FE2E864E310CB3" for 192.168.1.99/>]>
========================================================================
[FortigateAPI getVersion] -  https://192.168.1.99:443/api/v2/monitor/system/firmware
[FortigateAPI getVersion] version -  v6.0.2
[FortigateUpdate - main] - Device has been updated in version  v6.0.2
--- END ---
```


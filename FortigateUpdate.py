#!/usr/bin/env python3.7

import netmiko
import sys, os, time
import getopt
import socket
import FortigateAPI
from FortigateFirmwareFilename import getFortigateFirmwareFilename
from GetArguments import getOptForFortigateUpdate

# Const exit values
EXIT_FAILURE = 1
EXIT_SUCCESS = 0

def main (argv):

    arguments = dict()
    try:
        # Retrive arguments
        arguments = getOptForFortigateUpdate(argv)
    except Exception as error:
        print("[FortigateUpdate - main] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print("[FortigateUpdate - main] - Programm finish. Error in arguments")
        print("[FortigateUpdate - main] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        exit(EXIT_FAILURE)


    print("========================================================================")
    
    # Default values
    PortAreOpen = True

    # Test if Ports are Open (Default 443 and 22)
    PortAreOpen = checkIfPortOpen(arguments["ipAddress"], int(arguments["httpsPort"]))
    PortAreOpen = checkIfPortOpen(arguments["ipAddress"], int(arguments["sshPort"]))

    # Check if we use USB
    # If not check if port FTP or TFTP are open
    if arguments["usb"] is False:
        if arguments["ftpServerUse"] is True:
            # We will use FTP for download the new image
            PortAreOpen = checkIfPortOpen(arguments["ftpServer"], 21)
        else:
            # We will use TFTP for download the new image
            PortAreOpen = checkIfPortOpen(arguments["tftpServer"], 69)

    if PortAreOpen is False:
        print("[FortigateUpdate - main] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print("[FortigateUpdate - main] - Programm finish because ports are closed (1)")
        print("[FortigateUpdate - main] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        exit(EXIT_FAILURE)

    # Don't stop the programm if ping doesn't works
    # Firewall can block - we keep the information
    arguments["PING_WORKS"] = True if os.system("ping -c 1 " + arguments["ipAddress"]) is 0 else False

    print("[FortigateUpdate - main] PortAreOpen -", PortAreOpen)
    print("[FortigateUpdate - main] PING_WORKS  -", arguments["PING_WORKS"])

    try:
        # Create a object for use Fortigate API
        fortigate = FortigateAPI.FortigateAPI(arguments["username"], arguments["password"], arguments["ipAddress"], arguments["httpsPort"], useHTTPS=True)
        
        # Retrieve Model and Version with API (HTTPS)
        arguments["model"] = fortigate.getModel()
        arguments["actualVersion"] = fortigate.getVersion()
        print("[FortigateUpdate - main] Actual version and model- ", arguments["actualVersion"], arguments["model"])
        
        update(arguments)
    
    except Exception as error :
        print("[FortigateUpdate - main] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print("[FortigateUpdate - main] - Programm crash. No change was made (2)")
        print("[FortigateUpdate - main] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print(error)
        exit(EXIT_FAILURE)
            
    
    #### TEST IF UPDATE IS OK

    try:
        # Connect on API for check if after update version == version enter by user
        fortigate = FortigateAPI.FortigateAPI(arguments["username"], arguments["password"], arguments["ipAddress"], arguments["httpsPort"], useHTTPS=True)
        version = fortigate.getVersion()
    except Exception as error :
        print("[FortigateUpdate - main] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print("[FortigateUpdate - main] - Programm crash after updated Fortigate. No confirmation about update (3)")
        print("[FortigateUpdate - main] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        exit(EXIT_FAILURE)

    
    if (arguments["version"] in version):
        print("[FortigateUpdate - main] - Device has been updated in version ", version)
    else:
        print("[FortigateUpdate - main] - Device has not been updated in version ", arguments["version"])
        print("[FortigateUpdate - main] - Actuel device version is ", version)
        exit(EXIT_FAILURE)

    print("--- END ---")
    exit(EXIT_SUCCESS)

# --------------------------------------------------------------------------------------------------
#
#
#
def checkIfPortOpen (ipAdresses, port):
    '''

    return: True if all ports are open / False in all other cases
    '''

    print("[FortigateUpdate checkIfPortOpen] IP:Port -", ipAdresses, ":", port)

    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    print("[FortigateUpdate checkIfPortOpen] mySocket -", mySocket)

    result = mySocket.connect_ex((ipAdresses, port))
    if result == 0:
        print("[FortigateUpdate checkIfPortOpen] port", port, "is open")
        mySocket.close()
        return True
    else:
        print("[FortigateUpdate checkIfPortOpen] port", port, "is closed")
        mySocket.close()
        return False
# --------------------------------------------------------------------------------------------------
#
#
#
def update(arguments):
    '''

    '''
    print("########################################################################")

    # Set variables value
    fortigateNet = {
        'device_type': 'fortinet',
        'username': arguments["username"],
        'password': arguments["password"],
        'host': arguments["ipAddress"]
    }

    connector = netmiko.ConnectHandler(**fortigateNet)

    # Verify if user want update the device
    if arguments["update"] is True:
        # Retrive filename for download the file
        versionFilename, filenameOk = getFortigateFirmwareFilename(
            arguments["model"], arguments["version"])
        if filenameOk :
            print("[FortigateUpdate - update]", versionFilename, " For Fortigate ",
                  arguments["model"], " Version ", arguments["actualVersion"], " Upgrade to ", arguments["version"])

            if arguments['usb'] is True :
                command = "execute restore image usb " + str(versionFilename)
            elif (arguments['ftpServerUse'] is True) :
                command = "execute restore image ftp " + \
                    str(versionFilename) + " " + arguments["ftpServer"] + \
                    " " + arguments["ftpUser"] + " " + arguments["ftpPassword"]
            elif (arguments['tftpServerUse'] is True) :
                command = "execute restore image tftp " + \
                    str(versionFilename) + " " + arguments["tftpServer"]
            commandY = "y"
            print("[FortigateUpdate - update] command -", command)
            config_commands = (command,commandY)
            output = connector.send_config_set(config_commands)
            time.sleep(2)
            print("[FortigateUpdate - update] output  -", output)
            fortigateError(output)
            time.sleep(15)
            output = connector.find_prompt()
            fortigateError(output)
            # timeout below is used because fortigate is reachable when it downloads and upgrades
            # "Firmware upgrade in progress ..."
            time.sleep(300)
            # This variable is used for avoir an infinite loop
            count = 0
            while(count < 18):
                # If ping works, we will use ping for check if device is up and update is finished
                if arguments["PING_WORKS"] is True:
                    isUp = True if os.system("ping -c 1 " + arguments["ipAddress"]) is 0 else False
                # If not, we check if port 
                else:
                    isUp = True if checkIfPortOpen(arguments["ipAddress"], arguments["sshPort"]) is True else False

                if isUp is True:
                    print("[FortigateUpdate - update] Device is now reachable !")
                    # The timeout below is used for let time to http server to start
                    time.sleep(60)
                    break
                
                # Test each 30 secondes
                time.sleep(30)
                count = count + 1
                
                print("[FortigateUpdate - update] Device down ...")
            
        print("########################################################################")
        connector.disconnect()
        # END FUNCTION

def fortigateError(output):
    '''
    Check if the command contains
    '''
    error = ""
    errorB = True
    
    if "Return code -28" in output:
        error = "Can not get image from USB disk."
    elif "Return code -56" in output:
        error = "Incomplete command!"
    else:
        errorB = False
    
    if errorB is True:
        print("[FortigateUpdate - main] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print("[FortigateUpdate - main] - Programm finished.", error)
        print("[FortigateUpdate - main] ERROR - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        exit(EXIT_FAILURE)

    return
# --------------------------------------------------------------------------------------------------
#
#
#
if __name__ == "__main__":
   main(sys.argv[1:])
